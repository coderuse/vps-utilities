# Install OpenVPN

This is a copy from [Nyr/openvpn-install](https://github.com/Nyr/openvpn-install)

##Installation
Run the script and follow the assistant:

`wget --no-check-certificate https://goo.gl/5pwwi4 -O openvpn-install.sh && bash openvpn-install.sh`

Once it ends, you can run it again to add more users, remove some of them or even completely uninstall OpenVPN.


# Prepare VPS

This is my first preparation script for a new VPS

##Installation

`wget --no-check-certificate https://goo.gl/a3SZp2 -O prepare-vps.sh && bash prepare-vps.sh`

This is not a fully automated script and this changes the ssh port to 993. You may not want that. You can change the line#25 to `ufw allow 22/tcp`. If you have not modified, please change accordingly in the file `/etc/ssh/sshd_config`, `Port 993` instead of `Port 22`, before running the script.