#!/bin/sh

# Preliminary 'apt' updates & upgrades

apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade

echo "PS1='\n\[\033[01;34m\]\w\n\$ \[\033[00m\]'" >> ~/.bashrc

apt-get install screen curl ufw htop vim build-essential libssl-dev software-properties-common python-software-properties

# Install NodeJs via 'nvm'

curl https://raw.githubusercontent.com/creationix/nvm/v0.30.1/install.sh | bash
source ~/.profile
nvm install 5.4.1
nvm alias default 5.4.1
nvm use default

# Set 'ufw' defaults

ufw default deny incoming
ufw default allow outgoing
ufw allow 993/tcp
ufw allow 80/tcp
ufw allow 443/tcp
ufw enable

# Install Java 8

add-apt-repository ppa:webupd8team/java
add-apt-repository ppa:git-core/ppa
apt-get update
apt-get install oracle-java8-installer git
apt-get install oracle-java8-set-default

# Install 'golang'

wget https://storage.googleapis.com/golang/go1.6.2.linux-amd64.tar.gz
tar -xvf go1.6.2.linux-amd64.tar.gz
mv go /usr/local

# Set GO variables

mkdir -p $HOME/go/{bin,pkg,src/gitlab.com/arnabdas}
echo "export GOROOT=/usr/local/go" >> $HOME/.bashrc
echo "export GOPATH=$HOME/go" >> $HOME/.bashrc
echo "export PATH=$PATH:$GOROOT/bin:$GOROOT/bin" >> $HOME/.bashrc